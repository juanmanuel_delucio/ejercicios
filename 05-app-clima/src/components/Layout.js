import React from "react";

import { Button, Form, Grid, GridColumn, Header, Icon, Segment } from "semantic-ui-react";

import Api from "../utils/Api";

const Layout = () => {
   return (
      <>
         <Grid textAlign="center" style={{ height: "90vh" }} verticalAlign="middle">
            <Grid.Column style={{ maxWidth: 450 }}>
               <Header as="h1" color="blue" textAlign="center">
                  Search for Weather
               </Header>
               <Segment piled color="red">
                  <Grid>
                     <GridColumn>
                        <Form>
                           <Form.Input placeholder="City" />
                           <Button animated="vertical" color="green" className="submit" type="submit">
                              <Button.Content visible>Search</Button.Content>
                              <Button.Content hidden>
                                 <Icon name="search"></Icon>
                              </Button.Content>
                           </Button>
                        </Form>
                     </GridColumn>
                  </Grid>
               </Segment>
            </Grid.Column>
         </Grid>
      </>
   );
};

export default Layout;
