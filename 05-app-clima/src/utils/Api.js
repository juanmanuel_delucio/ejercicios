import axios from "axios";

const urlApi = "http://api.openweathermap.org/data/2.5/weather?units=metric&lang=es&q=";
const key = "140080a36026c48bf2db6a73046d8406";

const Api = {
   getWeather: async (city) => {
      const url = urlApi + city + "&appid=" + key;
      return await axios.get(url);
   },
};

export default Api;
