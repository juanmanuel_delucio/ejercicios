import React from "react";
import PropTypes from "prop-types";

const Saludo = ({ saludo, nombre }) => {
   return (
      <>
         <h1>{saludo}</h1>
         <p>{nombre}</p>
      </>
   );
};

Saludo.propTypes = {
   saludo: PropTypes.string.isRequired,
};

Saludo.defaultProps = {
   nombre: "Nombre por defecto",
};

export default Saludo;
