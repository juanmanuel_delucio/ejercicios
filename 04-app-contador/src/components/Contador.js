import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Axios from "axios";

const Contador = ({ value }) => {
   const [state, setstate] = useState(value);

   useEffect(() => {
      console.log("Contador Renderizado");
   }, []);

   useEffect(() => {
      if (state === 3) {
         console.log("Valor State: ", state);
         // const fetchData = async () => {
         //    const { data } = await Axios.get("https://jsonplaceholder.typicode.com/posts");

         //    console.log(data);
         //    data.forEach((respuesta) => {
         //       console.log(respuesta.id);
         //    });
         // };
         // fetchData();
      }
   }, [state]);

   const handlerAdd = () => setstate(state + 1);
   const handlerSubstract = () => setstate(state - 1);
   const handlerReset = () => setstate(5);

   return (
      <>
         <h1>Contador</h1>
         <h2>{state}</h2>

         <button onClick={handlerSubstract}> - 1</button>
         <button onClick={handlerReset}>Reset</button>
         <button onClick={handlerAdd}> + 1</button>
      </>
   );
};

Contador.propTypes = {
   value: PropTypes.number,
};
Contador.defaultProps = {
   value: 2,
};

export default Contador;
