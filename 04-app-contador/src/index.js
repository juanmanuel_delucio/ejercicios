import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
//import { App } from "./App";
//import Saludo from "./components/Saludo";
import Contador from "./components/Contador";

ReactDOM.render(<Contador value={2} />, document.getElementById("root"));
